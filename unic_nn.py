from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Read Data
input_file_path = 'TrainSet.csv'
df_input = pd.read_csv(input_file_path, sep=',', decimal='.')
# print dataframe
#print(df_input)

plt.plot(df_input['Prices.BE'].iloc[-(30*24):])
plt.ylabel('Prices BE')
plt.ylim(0, 200)


# Find missing values
missing_values_index_list = df_input[df_input['Prices.BE'].isnull()].index.tolist()
cnt_missing = len(df_input.loc[missing_values_index_list])
print(F"There are {cnt_missing} values missing.")

# Fix missing values
for i in missing_values_index_list:
    df_input.loc[i, 'Prices.BE'] = df_input['Prices.BE'].iloc[i - 168]

plt.plot(df_input['Prices.BE'].iloc[-(30*24):])
plt.ylabel('Prices BE')
plt.ylim(0, 200)
#plt.show()

# Handle Extreme Values
lower_limit = df_input['Prices.BE'].quantile(q=0.001)
upper_limit = df_input['Prices.BE'].quantile(q=0.999)
df_input['Prices.BE'] = df_input['Prices.BE'].clip(lower=lower_limit, upper=upper_limit)

# Plot clipped series
plt.plot(df_input['Prices.BE'])
plt.ylabel('Prices.BE')
plt.show()


# Additional Explanatory Variables
df_input['DateTime'] = pd.to_datetime(df_input['datetime_utc'])
df_input['Year'] = df_input['DateTime'].dt.year
df_input['Month'] = df_input['DateTime'].dt.month
df_input['Weekday'] = df_input['DateTime'].dt.dayofweek
df_input['Hour'] = df_input['DateTime'].dt.hour
df_input['Lag168'] = np.nan
df_input['Lag336'] = np.nan

for i in range(168, len(df_input)): # create lag 168
    df_input.loc[i, 'Lag168'] = df_input['Prices.BE'].iloc[i-168]

for i in range(336, len(df_input)): # create lag 336
    df_input.loc[i, 'Lag336'] = df_input['Prices.BE'].iloc[i-336]
    
df_input.dropna(axis=0, how='any', inplace=True)

# Split Data
fh = 168
# Todo continue data partioning